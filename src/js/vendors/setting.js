//menu header
$('.mobile-icon').on('click', function () {
	if ($(window).outerWidth() <= 1024) {
		$(this).toggleClass("mobile-close");
		$("html").toggleClass("is-locked");
		$(".header").toggleClass('active');
		$(".header-nav").toggleClass('active');
	}
});

$(window).on('load resize', function () {
	if ($(window).outerWidth() > 1024) {
		$('.mobile-icon').removeClass("mobile-close");
		$("html").removeClass("is-locked");
		$(".header").removeClass('active');
		$(".header-nav").removeClass('active');
	}
});

$(window).on('load scroll', function () {
	if ($(window).scrollTop() > 0) {
		$(".header").addClass('is-fixed');
	} else {
		$(".header").removeClass('is-fixed');
	}
	
	if ($(window).scrollTop() + $(window).outerHeight() >= $('.footer').offset().top) {
		$('.header-btn').hide();
	} else {
		$('.header-btn').show();
	}
});

$(function () {
	objectFitImages('img');
});

//matchHeight
// jQuery(function ($) {
//     $('.news .item').matchHeight();
// });


//fade
$(window).on('scroll load assessFeatureHeaders', function () {
	var scrollTop = $(window).scrollTop();
	var appearenceBuffer = 60;
	var windowBottom = scrollTop + $(window).height() - appearenceBuffer;
	$('body').toggleClass('scrolled-down', scrollTop > 0);
	$('.js-scale:not(.active)').filter(function () {
		var offset = $(this).offset().top;
		var height = $(this).outerHeight();
		return offset + height >= scrollTop && offset <= windowBottom;
	}).addClass('active');
});

$(document).ready(function () {
	
	$('.list-recruittabs li:first-child').addClass('active');
	$('.item-recruitct').hide();
	$('.item-recruitct:first').show();

// Click function
	$('.list-recruittabs li').click(function () {
		$('.list-recruittabs li').removeClass('active');
		$(this).addClass('active');
		$('.item-recruitct').hide();
		
		var activeTab = $(this).find('a').attr('href');
		$(activeTab).fadeIn();
		return false;
	});
	
}); // end ready

/* -----------------
	write slick slider here because homepage doesn't have slick slider
------------------ */
if($('.leader-list').length) {
	$('.leader-list').slick({
		arrows: true,
		focusOnSelect: true,
		pauseOnHover: false,
		infinite: true,
		speed: 700,
		dots: false,
		autoplay: false,
		cssEase: 'linear',
		centerMode: true,
		centerPadding: '18.8%',
		slidesToShow: 1,
		prevArrow: '<div class="slick-prev"></div>',
		nextArrow: '<div class="slick-next"></div>',
		responsive: [
			{
				breakpoint: 834,
				settings: {
					centerPadding: '10%',
				}
			}
		]
	});
}